session s42:
	npm init -y
	npm install express mongoose cors
	npm install bcrypt
	npm install jsonwebtoken
	create data models for users, products, orders
	create a route for user registration
	create a route for user authentication

session s43:
	create a route for add product
	create a route for retrieving all products
	create a route for retrieving active products

session s44:
	create a route for retrieving a single product
	create a route for updating a product (accessible by only admin)
	create a route for archiving a product (accessible by only admin)

session s45:
	create a route for activating a product
	create a route for ordering products
	create a route for retrieving user details
	create a route for retrieving all orders(only by admin)
	create a route for retrieving a user orders
	create a route for updating user as admin(only by admin)
	create a route for adding product to cart 
	create a route for retrieving added products in cart 
	
