const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const Order = require("../models/Order");
const Cart = require("../models/Cart");

module.exports.registerUser = (reqBody) => {
	console.log(reqBody);

	return User.findOne({ email: reqBody.email }).then(result => {

		if(result == null){
			let newUser = new User({
			name: reqBody.name,
			email: reqBody.email,
			password: bcrypt.hashSync(reqBody.password, 10),
			mobileNo: reqBody.mobileNo,
			age: reqBody.age
		})

		return newUser.save().then((user, error) => {

			if(error){
				return false
			} else{
				return true
			}

		})
		} else{
			return "An account already exists with this email."
		}
	})
}

module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {

		if(result == null){
			return "User does not exist."
		} else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			console.log(isPasswordCorrect);
			console.log(result);

			if(isPasswordCorrect){
				
				//return `${result.name}, successfully logged in.`
				//return result

				return { access: auth.createAccessToken(result)}

			} else{
				return "Password is incorrect"
			};
			
		}	

	})

}


module.exports.checkOut = async (data) => {

	if(!data.isAdmin){

		let amount =0;
		const products = data.order.products;
		for(const product of products){
			let productDetails = await Product.findById(product.productId).then(result => {
				console.log(result);
				return result;
			});
			if(!productDetails.isActive){
				return false;
			}
			amount+=(productDetails.price) * (product.quantity)
		};
		let newOrder = new Order({
				userId: data.userId,
				products: data.order.products,
				totalAmount: amount
			});

			console.log(newOrder);
			return newOrder.save().then((order, err) => {
				if(err){
					return false;
				} else{
					return true;
				}
			})
		
	}
	let message = Promise.resolve("A non admin user is only allow to access this!");
	return message.then((value) => {
		return value
	});

}

module.exports.getUserDetails = (reqParams) => {
	
		return User.findById(reqParams.userId).then(result => {
			if(result==null){
				return false;
			} else{
				result.password="";
				return result;
			}
		})
}


module.exports.getAllOrders = (isAdmin) => {

	if(isAdmin){
		return Order.find({}).then(result => result);
	} 

	let message = Promise.resolve(false);
	return message.then(value => value);
};


module.exports.getMyOrders = (isAdmin, idOfuser) => {
	console.log(isAdmin);
	if(!isAdmin){

		console.log(idOfuser);
		return Order.find({userId: idOfuser}).then(result => result);
	} 

	let message = Promise.resolve(false);
	return message.then(value => value);
};


module.exports.setUserAsAdmin = (reqParams, isAdmin) => {
	if(isAdmin){
		let updatedUser= {
			isAdmin: true
		};
		
		return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((result, err)=> {
			//console.log(reqParams.userId);
			if(err){
				return false;
			} else{
				return true;
			}
		});
	} 

	let message = Promise.resolve("Only admin can access this!");
	return message.then((value) => {
		return value;
	});
};


module.exports.getproductsIncart = (isAdmin, idOfuser) => {
	
	if(!isAdmin){

		return Cart.find({userId: idOfuser}).then(result => result);
	} 

	let message = Promise.resolve(false);
	return message.then(value => value);
};


module.exports.addToCart = async (data, reqBody) => {
	if(!data.isAdmin){
		let cartItem = await Cart.findOne({userId:data.userId}).then((result) => {
			return result;
		});
		if(cartItem==null){
		let amount=0, total=0;
		
		let newProducts = new Cart({
				userId: data.userId,
				totalAmount: 0
			});
		const products = reqBody.products;
	
		for(const product of products){
			let productDetails = await Product.findById(product.productId).then(result => {
				console.log(result);
				return result;
			});
			if(!productDetails.isActive){
				return false;
			}
			amount=(productDetails.price) * (product.quantity);

			newProducts.products.push({
				productId: product.productId,
				quantity: product.quantity,
				subtotal: amount
			})
			total = total+amount;
			

		};
		
			newProducts.total= total;
			return newProducts.save().then((order, err) => {
				if(err){
					return false;
				} else{
					return true;
				}
			})
		} else{
			
			const products = reqBody.products;
			//const productInCart = await cartItem.products.find(product => {product.productId === reqBody.productId});
			
			
				let amount = 0;
				for(const product of products){
					/*console.log(product.productId);
					console.log(cartItem.products);
					cartItem.products.forEach(item => {
						if(product.productId === item.productId){
							console.log(item.productId);
							console.log("Hi");
						}
					})*/
					const productInCart = await cartItem.products.find((item) => item.productId === product.productId);
					//console.log(productInCart);
					const item = await Product.findById(product.productId);
						
					if(!productInCart){
						
						subtotal = (item.price)*(product.quantity);
						cartItem.products.push({
							productId: product.productId,
							quantity: product.quantity,
							subtotal: subtotal
						})
					}
					else{
						/*subtotal = productInCart.subtotal
						quantity = productInCart.quantity
						subtotal += (product.quantity )*(item.price);
						quantity += product.quantity;

						cartItem.save(product.productId, ({quantity: quantity}, {subtotal: subtotal})).then((cart, err) => {
							if(err){
								return false;
							} else{
								return true;
							}
						})*/
						return "Product exists in Cart."
					}

					amount += subtotal;

					
				}
					cartItem.totalAmount += amount;
					return cartItem.save().then((cart, err) => {
					if(err){
							return false;
						} else{
							return true;
						}
					})
					
				}
		
			

			
		
	}
	
	let message = Promise.resolve("A non admin user is only allow to access this!");
	return message.then((value) => {
		return value
	});
}


module.exports.deleteProductInCart = async (data, reqParams) => {

	let userExist = await Cart.findOne({userId: data.id});
	if(userExist){
		let productindex = userExist.products.findIndex((item) => item.productId === reqParams.productId);
		if(productindex === -1){
			return "Product is not in cart"
		} else{
			userExist.totalAmount -= userExist.products[productindex].subtotal;
			userExist.products.splice(productindex, 1);

			return userExist.save().then((res, err) => {
				if(err){
					return false;
				} else{
					console.log(userExist);
					return "Removed Product from cart!"
				}
			})
		}
	} else{
		return "No items in cart"
	}
};

module.exports.updateQuantity = async (data, reqParams, reqBody) => {

	let userExist = await Cart.findOne({userId: data.id});
	if(userExist){
		let productindex = userExist.products.findIndex((item) => item.productId === reqParams.productId);
		let product = await Product.findById(reqParams.productId);
		console.log(product);
		if(productindex === -1){
			return "Product is not in cart"
		} else{
			userExist.totalAmount -= userExist.products[productindex].subtotal;
			userExist.products[productindex].subtotal = (reqBody.quantity) * (product.price);
			userExist.products[productindex].quantity = reqBody.quantity;
			userExist.totalAmount += userExist.products[productindex].subtotal;
			
			console.log(userExist);
			return userExist.save().then((result, err) => {
				if(err){
					return false;
				} else{
					return "Update successful!";
				};
			});
			
		}
	} else{
		return "No items in cart"
	}
}


