const Product = require("../models/Product");

module.exports.addProduct = (data) => {

	//console.log(data);

	if(data.isAdmin){

		let newProduct = new Product({
			productName: data.product.productName,
			description: data.product.description,
			price: data.product.price
		});

		return newProduct.save().then((product, error) => {

			if(error){
				return false;
			} else{
				return "The product is successfully added.";
			}
		})
	} 

	let message = Promise.resolve("User must be an Admin to access this!");
	return message.then((value) => {
		return value
	});
	
};


module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {
		return result;
	});
};


module.exports.activeProducts = () => {

	return Product.find({ isActive: true }).then(result => result);
};


module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {

	if(isAdmin){
		let updatedProduct = {
			productName: reqBody.productName,
			description: reqBody.description,
			price: reqBody.price
		};

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, err) => {
			if(err){
				return false;
			} else{
				return "successfully updated!"
			};
		});
	} 

	let message = Promise.resolve("User must be an Admin to access this!");
	return message.then((value) => {
		return value
	});
};


module.exports.archiveProduct = (reqParams, isAdmin) => {

	if(isAdmin){
		let updateActiveField = {
			isActive: false
		}

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, err) => {
			if(err){
				return false;
			} else{
				return "Successful";
			};
		});
	}

	let message = Promise.resolve("User must be an Admin to access this!");
	return message.then((value) => {
		return value
	});
};

module.exports.activateProduct = (reqParams, isAdmin) => {

	if(isAdmin){
		let updateActiveField = {
			isActive: true
		}

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, err) => {
			if(err){
				return false;
			} else{
				return "Successful";
			};
		});
	}

	let message = Promise.resolve("User must be an Admin to access this!");
	return message.then((value) => {
		return value
	});
};