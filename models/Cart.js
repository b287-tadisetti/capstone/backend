const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	userId: {
		type:String,
		required: [true, "user Id is required."]
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "product Id is required."]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required."]
			},
			subtotal: {
				type: Number,
				required: [true, "subtotal for each item is required"]
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, "Total amount is required."]
	},
	
})

module.exports = mongoose.model("Cart", cartSchema);