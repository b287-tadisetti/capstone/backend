const express = require("express");
const mongoose = require("mongoose");

const userRoutes = require("./routes/userRoute");
const productRoutes = require("./routes/productRoute");

const cors = require("cors");

const app = express();

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.9m0payg.mongodb.net/capstone2", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

mongoose.connection.once("open", ()=> console.log('We are connected to the cloud.'))

app.use(express.json());
app.use(express.urlencoded({ extended: true}));


app.listen(process.env.PORT || 5000, () =>{
	console.log(`API is now online in the port ${process.env.PORT || 5000}`)
})


app.use("/users", userRoutes);
app.use("/products", productRoutes);