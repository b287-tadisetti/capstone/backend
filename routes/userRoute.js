const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

//Route for user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for user authentication 
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for ordering a product
router.post("/check-out", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		order: req.body
	}

	userController.checkOut(data).then(resultFromController => res.send(resultFromController));
});

//Route to retrieve user details
router.get("/:userId/userDetails", auth.verify, (req, res) => {
	
	userController.getUserDetails(req.params).then(resultFromController => res.send(resultFromController));

});

//Stretch goals

//Route to retrieve all orders
router.get("/orders", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin

	userController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));

});


//Route to retrieve user's orders
router.get("/myOrders", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	const idOfuser = auth.decode(req.headers.authorization).id

	userController.getMyOrders(isAdmin, idOfuser).then(resultFromController => res.send(resultFromController));

});


//Route to set user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	userController.setUserAsAdmin(req.params, isAdmin).then(resultFromController => res.send(resultFromController));

});

//Route to add products in cart
router.post("/addtocart", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.addToCart(data, req.body).then(resultFromController => res.send(resultFromController));
})

//Route to get added products in cart
router.get("/addedproducts", auth.verify, (req,res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	const idOfuser = auth.decode(req.headers.authorization).id

	userController.getproductsIncart(isAdmin, idOfuser).then(resultFromController => res.send(resultFromController));
})

//Route to remove products from cart
router.delete("/:productId/cart", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	userController.deleteProductInCart(data, req.params).then(resultFromController => res.send(resultFromController));
});

//Route to change the quantity of products in cart
router.put("/:productId/updatequantity", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	userController.updateQuantity(data, req.params, req.body).then(resultFromController => res.send(resultFromController));
});



module.exports = router;