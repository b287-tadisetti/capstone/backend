const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

//Route for creating a product(Admin only)
router.post("/", auth.verify, (req, res) =>{

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

//Route for retrieving all the products
router.get("/all", (req, res) => {

	productController.getAllProducts().then(resultFromController => res.send(resultFromController));

});

//Route for retrieving all the ACTIVE products
router.get("/active-products", (req, res) => {

	productController.activeProducts().then(resultFromController => res.send(resultFromController));

});

//Route for retrieving a specific product 
router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

});

//Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));

});

//Route for archiving a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	productController.archiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));

});

//Route for activating a product
router.put("/:productId/activate", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	productController.activateProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));

});


module.exports = router;